{
  const {
    html,
  } = Polymer;
  /**
    `<cells-ernesto-dashboard>` Description.

    Example:

    ```html
    <cells-ernesto-dashboard></cells-ernesto-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-ernesto-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsErnestoDashboard extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-ernesto-dashboard';
    }

    static get properties() {
         return {
           vist: {
             type: Object,
             value:{}
           },
           data: {
             type: String
           }
          };
        }
        Filter(string) {
          if (!string) {
            return null;
          } else {
            string = string.toLowerCase();
            return function(alumnosArry) {
              var name = alumnosArry.name.toLowerCase();
              var last = alumnosArry.last.toLowerCase();
              return (name.indexOf(string) != -1 || last.indexOf(string) != -1);
            };
          }
        }
      static get template() {
      return html `
      <style include="cells-ernesto-dashboard-styles cells-ernesto-dashboard-shared-styles"></style>
      <slot></slot>
        <cells-mocks-component alumnos = "{{vist}}"></cells-mocks-component>
        
      <iron-ajax
      auto
      id ="req"
      url="https://api.chucknorris.io/jokes/random"
      method = "get"
      handle-as="String"
      last-response="{{data}}"></iron-ajax>

      <ul class="app-grid">
        <div class="input-group" style="width: 100%; display: inline-block;">
          <h4>Buscar por nombre:</h4>
          <input value="{{searchString::input}}" class="form_2" label="Ingresa el nombre a buscar">
        </div>

        <div  >
          <blockquote>Chuck dice: [[data.value]]</blockquote>
        </div>

        <template  is="dom-repeat" items= "{{vist}}" filter="{{Filter(searchString)}}">
          <div class="centered-container  card_2 degradado">
            <div class="nom"><span>{{item.name}} {{item.last}}</span></div><br>
            <div class="direc"><samp>{{item.adress}}</samp></div><br>
            <div class="hobi"><samp>{{item.hobbies}}</samp></div>
          </div>
        </template>
      </ul>
      `;
    }
  }
  customElements.define(CellsErnestoDashboard.is, CellsErnestoDashboard);
}
